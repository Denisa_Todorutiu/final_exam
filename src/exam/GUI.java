package exam;

    public class GUI implements Runnable {

        public void run(){
            Thread t = Thread.currentThread();
            //for(int i=0;i<5;i++) ca sa afiseze in total 10 mesaje
            for(int i=0;i<10;i++){
                System.out.println(t.getName() + " i = "+i);
                try {
                    Thread.sleep( 6000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(t.getName() + " DONE.");
        }

        public static void main(String[] args) {
            GUI c1 = new GUI();
            GUI c2 = new GUI();


            Thread t1 = new Thread(c1,"SE1-Thread1");
            Thread t2 = new Thread(c2,"SE2-Thread2");


            t1.start();
            t2.start();

        }
    }



